---
# See `man pandoc | grep "Metadata variables"`
title: Best Practice (for an OSH project)
# These two are supplied their final value from `git describe`
subtitle: "*version:* [${PROJECT_VERSION}](${PROJECT_VERSION_URL})"
version: "${PROJECT_VERSION}"
date: "${PROJECT_VERSION_DATE}"
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
keywords:
- Open Source Hardware
- Best Practice
- DIN SPEC 3105-1
subject: Best Practise
category: Auxilliary Document
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
description: >
  English draft of the Deutsche Industrie Norm for open source hardware -
  Best Practice (for an OSH project)
table-of-contents: true
breaks: false
---

<!--
SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

<!--
About *table of contents* (TOC)

NOTE:
For each in-document command,
make sure that there is a blank line before and after.

Support in different Markdown software:

* CodiMD, HedgeDoc & other web based Markdown tools
  support the in-document `[TOC]` command
* GitLab supports the in-document `[[_TOC_]]` command
* Kramdoc supports the in-document `{:toc max_level=3 }` command
* GitHub does not support anything,
  see https://github.com/isaacs/github/issues/215#issuecomment-561605313
* pandoc supports the `\-\-toc` command-line flag,
  which will put a TOC on the start of the document
-->

[TOC]

[[_TOC_]]

# Licensing

## When should I choose my license?

This is arguably the most important question in this document,
so if you read this, you are on the right track.
Thank you!

In Open Source, you choose the license in the very beginning,
always, without any exception, ever!
You choose the license,
add the respective *LICENSE.md* file to your repo
as the very first file at all.
This is the optimal way,
saving you from lots of trouble down the road.

You may choose to make the repo public right away,
or at a later point.

## How to choose my license?

*Short Answer:* Use **CERN OHL v2**.

*Long Answer:*\
Choose a hardware specific licence;
Not CC-BY-SA, not MIT, not GPL.

The simplest way,
is to choose your flavour of CERN OHL v2:

* permissive (-p)
* weak (-w)
* strong copyleft (-s)

according to the question:\
How much do you want to prevent proprietary derivatives of your work?

### More in-depth

To be OSH, in principle,
you can choose any [supported license](#which-licenses-may-i-use-for-my-project).

Though, you may only freely choose your license,
if you do not depend on anything else,
be it in software or hardware.
If you *do* depend on anything else,
you have to respect the license of these dependencies
in your project in general,
and especially when choosing your projects license.
That may limit you in your choices.

It is a complex topic,
and you should probably consider learning about the options,
consulting other resources online.\
Do not take this too lightly.

For even more info relevant for choosing a license,
please check [this amazing OH licenses comparison](
https://gitlab.com/unixjazz/oh-licenses-contrasts),
[CERN OHL's FAQ](
https://ohwr.org/project/cernohl/wikis/faq)
or their [rationale](
https://ohwr.org/project/cernohl/wikis/uploads/0be6f561d2b4a686c5765c74be32daf9/CERN_OHL_rationale.pdf).

> NOTE\
> It would be great to have something akin
> [the CreativeCommons license chooser](https://creativecommons.org/choose/)
> for OSH [(related issue)](https://gitlab.com/OSEGermany/OHS/-/issues/50).\
> \... in case you are looking for a project idea.

# Documentation

## General documentation advices

### When should I write the documentation?

Very good question,
Thank you!

In Open Source,
the right moment for both,
[choosing a license](#when-should-i-choose-my-license)
and starting the documentation is right in the beginning!
Always, always, always!

### Which file formats and software should I use for documentation?

A lot of consideration has gone into this,
and the optimal setup we came up with is this:

* **git** repo hosted (usually within the projects main repo)
* using the **Markdown** file format
* using only Markdown features that are supported by both
  the [GitHub-](https://help.github.com/articles/github-flavored-markdown/) and
  the [Pandoc-flavor](http://pandoc.org/README.html#pandocs-markdown) of Markdown,
  which is almost equal to the *GitHub-flavor (GFM)*

This approach:

* is very flexible
* allows for a high degree of freedom in the choice of editing software
  * GitHub/GitLab web-interface
  * GitHub/GitLab Wiki-interface
  * Any desktop/smart-phone based Markdown editor
    (there are thousands)
  * Any web-based Markdown editor
    (like [HedgeDoc](https://github.com/codimd/server)
    or [CodiMD](https://github.com/hackmdio/codimd))

The absolute minimum would be,
to supply documentation in an open, editable format
for which open editing software is available
for all commonly used platforms
(Windows, Linux, OSX, Android, iOS).

### How should I document the projects meta-data?

As of now (August 2020),
the recommended way to do that,
is to keep an [Open Know-How (OKH) meta-data file](
https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1)
in your projects root directory, named *okh.yml*.

You may want to check out these examples:

* <https://raw.githubusercontent.com/case06/upklib_v2/master/okh.yml>
* <https://raw.githubusercontent.com/case06/ZACplus/master/okh-ZACplus.yml>
* <https://gitlab.com/OSEGermany/ohloom/-/raw/master/okh.toml>

> NOTE\
> We are already working on an extended/improved version of this format,
> and will provide tooling to easily convert to newer formats,
> once it is released.

## (Technology-)specific documentation advices

### Bill of Materials

Whether working with the assembly shop down the street or to enable
decentralised mass production, a clear and complete Bill of Materials (BoM) is
the first step to specify your design.\
The BoM can be a fairly simple table listing all components of an assembly,
their quantity and unambiguous reference in structural form. Which means that
only from the BoM people should see which components go into subassemblies.\
Let's say we have some brilliant machine consisting of 5 components and a
subassembly (pos. 5), a very simple BoM (using standardised metadata) would look
like this:

| Pos. | Name         | Units | TsDC-ID | Reference                                                                     |
|------|--------------|-------|---------|-------------------------------------------------------------------------------|
| 1    | casing       | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 2    | screw        | 4     | COM-STD | hexagon screw ISO 4017 – M6 × 35 – 8.8                                        |
| 3    | gear box     | 1     | COM-OSH | link to corresponding release [OSH-Module](ohs-3105-3#osh-module)             |
| 4    | Raspberry Pi | 1     | COM-BUY | unambiguous reference (not standardised)                                      |
| 5    | holder       | 1     | ASM-MEC | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 5.1  | bracket      | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 5.2  | screw        | 2     | COM-STD | hexagon screw ISO 4017 – M4 × 12 – 5.6                                        |
| 5.3  | arm          | 2     | COM-MAN | link to [POSH file](ohs-3105-3#piece-of-osh-posh) which links to design files |
| 6    | controller   | 1     | COM-OSH | link to corresponding release [OSH-Module](ohs-3105-3#osh-module)             |

It's clear that the bracket (pos. 5.1), the 2 screws (pos. 5.2) and the arm
(pos. 5.3) are to be assembled to make the holder (pos. 5).

However, to facilitate production there are dozen ways to optimise your BoM,
which will make things far easier for your users. Some very useful information
to add are:

* approved manufacturer(s);
* tolerance, material composition and other specific specs;
* package type information;
* extended part numbers specific to manufactures.

For details, especially for electronics, check this great source: <https://www.bunniestudios.com/blog/?p=2776>.

# Storage

## How should I store/host/organize my projects files, including the documentation?

Our strong recommendation is to store everything
in a [git](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F) repo,
or optionally in multiple ones,
if you have a large project made up of multiple,
somewhat independent modules.

The reasons we recommend git, are:

* it has built-in, first-class collaboration mechanisms
* it natively supports a P2P workflow
  * making backups is very easy
  * maliciously "killing" a project is very hard
  * reviving a died-out project is very easy
* it is the de-facto standard in Open Source software development
  ([for good reason](https://www.atlassian.com/git/tutorials/what-is-git))
* it is open source
* there are many good, open source tools for it
* it is very performant
* it stores the whole history of a project development,
  not just the latest state

Counter arguments and re-counter arguments:

* git is difficult to learn
  * true, but it is the last such thing you\'ll have to learn,
    it is that good,
    and there are lots of tools to make parts of it easy
* git is bad for binary files, which are very common in hardware projects
  * many of its strengths indeed fail totally in the case of binary files,
    but it does not fare worse then other tools that store every change in a project,
    and for many binary files there are [workarounds](#what-about-binary-files)

## What about binary files?

> *Short answer:*\
> `git` is not well suited for handling binary files,
> but still the best we have
> and thus what we recommend.

Yes, it is true what you heard:
git does *not* fare well with binary files.

But what does that really mean?

Git has a lot of benefits over other systems,
when it comes to the handling of text-based files:

* it uses less storage space (Mega Bytes)
* because of the above, it also has performance benefits
  when loading from storage and transferring over the network
* it can show the differences between commits (chunks of changes)
  in a meaningful way
* it can (mostly) merge different changes in the same file automatically
* ... if not, it gives hints for how to do it manually

All of these do not apply to binary files.
That said,
it does not really fare worse with binary files
then other systems that allow storing all changes
of a set of files over time.

In our view, this comes down to this simple ranking (from best to worst):

1. Git without binary files
2. Git with binary files
3. *[some other system]* without binary files
4. *[some other system]* with binary files

## How should I actually deal with binary files?

Given you are using `git`,
there are a number of things you can do
to better performance and user experience
regarding binary files in your repo:

1. **Avoid them**
  * Never store generated files in the repo its self,
    no matter how easy and tempting.
    Rather try auto-generating them with a script
    which is stored in the repo,
    and which you then execute in your [CD job](
    https://en.wikipedia.org/wiki/Continuous_delivery).
    This is more work in the beginning,
    but has many benefits over time,
    and you will usually be able ot copy&paste this solution
    to other projects.
  * Try to replace binary files with text-based ones.
    For example, use SVG instead of PNG,
    or an Markdown file or at least an XML based LibreOffice file
    instead of a binary one,
    whenever possible.
  * If they are non-essential,
    think about storing them somewhere outside the repo.
2. **Minimize their size**
3. **Make them *quasi textual***\
   Git supports a number of workflows,
   that can be used to mitigate a lot of the issues
   with binary files.\
   There are these basic issues one has to consider
   for binary files in git
   (from easiest to hardest problem):
   1) no (human readable, textual) representation
   2) no (human readable, textual) diffs
   3) no intuitive, graphical representation (this is not binary exclusive)
   4) no intuitive, graphical diffs (this is not binary exclusive)
   5) no delta-compression compatibility \
     -> bad storage space behavior
   6) no possibility to merge

   All of these really exist locally on the users machine,
   and mostly also on the common centralized git hosters web interfaces.
   As we promote a P2P workflow,
   I will concentrate on the local versions from here on.

   Problems 1. to 2. are technically somewhat easily solved
   for most binary files,
   just by fusing together a few already existing tools
   in a short script.
   <!-- TODO Add examples -->

   Similar can be said about 3. and 4. .
   <!-- TODO Add examples -->

   Whether 5. is solvable,
   depends on whether there is a lossless way of conversion
   from the binary file into a text based,
   or at least into an uncompressed, unmangled and "serial" format,
   and back again.\
   To our knowledge, this is possible for:
   * all ZIP based files with [ReZipDoc](https://github.com/hoijui/ReZipDoc),
     which includes at least:
     * FreeCAD
     * LibreOffice binary
     * ZIP
     * JAR
     * MS Office binary
   * PNG files with [png-inflate](https://github.com/rayrobdod/png-inflate)

   Problem 6. is by far the hardest,
   as on top of all the requirements of 5.,
   it adds an other one,
   which is mergeability of the converted format.\
   We do not know of any tools that support this for any formats.

# Community

## How to lead a community?

We highly recommend to be inspired by
[this video of Pieter Hintjens](
https://www.youtube.com/watch?v=QB7VXSc5H3A)
(founder of the ZeroMQ software),
where he talks about his view and vision
of the Open Source community
as a distributed "Living System",
including very practical tips for community leaders.

*Summary:*

* **Trust**
* **Encourage**
* **Give freedom**
* **Minimize bureaucracy**

Fuel good vibes in any potential contributor in any way possible;
as any changes to the (digital) project can easily be reverted
any time later.

<!--
  TODO Add References, maybe youtube videos or statements
  of prominent figures like [Pieter Hintjens](
  https://en.wikipedia.org/wiki/Pieter_Hintjens)
  (if applicable)
-->

# Certification

## In what stage should I try to get my project certified?

When you feel confident that others would be able to build your hardware,
given your current state of documentation,
and your repo contains [a *okh.yml* meta-data file](https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1).

See also:
[When should I write the documentation?](#when-should-i-write-the-documentation)

## Which licenses may I use for my project?

If possible,
follow the [suggestion given earlier](#how-to-choose-my-license).

If you need more options to choose form,
please go to [the OSHWA FAQ](https://www.oshwa.org/faq/),
and search for the question titled "*What license should I use?*".

> NOTE:\
> There is [an open issue](https://community.oshwa.org/t/website-repository/546/2)
> for adjusting the OSWHA FAQ,
> so we would be able to link directly to the relevant question.
