---
# See `man pandoc | grep "Metadata variables"`
title: FAQ
# These two are supplied their final value from `git describe`
subtitle: "*version:* [${PROJECT_VERSION}](${PROJECT_VERSION_URL})"
version: "${PROJECT_VERSION}"
date: "${PROJECT_VERSION_DATE}"
lang: en-US
charset: UTF-8
license: CC-BY-SA 4.0
keywords:
- Open Source Hardware
- FAQ
- DIN SPEC 3105
- DIN SPEC 3105-1
- DIN SPEC 3105-2
subject: FAQ
category: Auxilliary Document
papersize: a4
geometry: "top=2cm,bottom=2cm,left=3cm,right=3cm"
description: >
  English draft of the Deutsche Industrie Norm for open source hardware -
  FAQ
table-of-contents: true
breaks: false
---

<!--
SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
SPDX-FileCopyrightText: 2020 Jérémy Bonvoisin <jbjb2971@bath.ac.uk>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

<!-- markdownlint-disable MD025 -->
<!-- markdownlint-disable MD026 -->

<!--
About *table of contents* (TOC)

NOTE:
For each in-document command,
make sure that there is a blank line before and after.

Support in different Markdown software:

* CodiMD, HedgeDoc & other web based Markdown tools
  support the in-document `[TOC]` command
* GitLab supports the in-document `[[_TOC_]]` command
* Kramdoc supports the in-document `{:toc max_level=3 }` command
* GitHub does not support anything,
  see https://github.com/isaacs/github/issues/215#issuecomment-561605313
* pandoc supports the `\-\-toc` command-line flag,
  which will put a TOC on the start of the document
-->

[TOC]

[[_TOC_]]

# About this standard

## DIN SPEC 3105, what is this?

DIN SPEC 3105 is a standard that delivers an explicit definition
of the term "Open Source Hardware" (OSH),
based on objective and enforceable criteria.
This means, it defines precise criteria to make the difference
between hardware devices qualifying as OSH
and those who don't.
This standard extends the
["Open Source Hardware Definition 1.0"](https://www.oshwa.org/definition/)
hosted by the Open Source Hardware Association --
a definition that is quite generally accepted in the field of OSH.
In short, it delivers more detail where the
"Open Source Hardware Definition 1.0" remained vague,
for example when it comes to define OSH documentation contents.

DIN SPEC 3105 has two parts:

- Part 1 is about defining the concept of "Open Source Hardware".
  This document is entitled "Open Source Hardware - Requirements for documentation"
  and sets requirements to be fulfilled by the documentation of a piece of hardware
  in order to qualify as "Open Source Hardware".
  It takes over the licensing requirements defined in the
  ["Open Source Hardware Definition 1.0"](https://www.oshwa.org/definition/)
  and adds precise requirements
  in terms of documentation contents, availability and format.
- Part 2 is about certifying hardware as open source.
  This document is entitled "Open Source Hardware - Community-based assessment"
  and sets requirements for a community-based certification procedure.
  The idea is to mimic the peer-review process used in scientific publishing,
  so that certifications are not granted by a central entity
  but by the community itself.
  This is -- as far as we know --
  a quite unique model in the field of product certification procedures,
  but that's a model that is very valuable
  because it fits well the the ethos of the OSH communities.

## What is new in this standard?

The main original contributions of DIN SPEC 3105 compared to existing standards are:

- It breaks down the requirement made by the
  ["Open Source Hardware Definition 1.0"](https://www.oshwa.org/definition/)
  to enable "anyone \[to\] study, modify, distribute, make, and sell
  the design or hardware"
  into clear, actionable ways to meet that requirement.
  Therewith, it defines **four "Rights of Open Source"**
  transposing the ["four essential freedoms"](https://www.gnu.org/philosophy/free-sw.html)
  stated in the Free Software Definition
  and links them with concrete requirements in terms of documentation content.
  These rights are the right to study, to modify, to make, and to distribute.
- It acknowledges that OSH is not only a matter of licensing
  but also a matter of **documentation content**.
  It differentiates between granting the four rights of open source
  and enabling others to more effectively exercise these rights.
  Many people have successfully exercised rights
  to reproduce and build upon OSH products
  but DIN SPEC 3105-1 addresses the issue
  that existing standards have limited guidance
  on what documentation should contain
  in order to ensure compliance.
- It acknowledges that the content of the documentation to be shared
  depends on the hardware **technologies** embedded in the product under consideration
  (e.g. electronic vs mechanical hardware).
  To account for these dependencies,
  the specification only dictates generic requirements
  in terms of documentation contents.
  These contents are in turn specified by so-called
  ["Technology-specific Documentation Criteria"](https://gitlab.com/OSEGermany/oh-tsdc)
  which are specific for each technology.
- It acknowledges that the content of the documentation
  also depends on the **audience** targeted by the documentation.
  It is not practicable to allow
  "anyone [to] study, modify, distribute, make, and sell the design or hardware"
  since not everyone has the same capacity to read and make use of product documentation.
  Instead, documentation is required to address a defined group of recipients.
  A documentation must provide at minimum the information
  that specialists in the relevant field of technology would require
  to exercise the four rights of open source hardware -
  this is analogous to the requirement in patent law
  that applicants must provide sufficient information for implementation
  by those "skilled in the art".
  Projects can of course exceed this minimal expectation
  of documentation detail and completeness
  and benefit wider users by making this aim explicit.
- It adopts a **product life cycle perspective**,
  considering that "making" a product is not only about manufacturing it,
  it is also about using, maintaining, updating and processing it at end-of-life.
  It defines documentation as information allowing to operate all activities
  belonging to the product life cycle,
  from raw material extraction to end-of-life.
  Therewith, it removes the arbitrary and artificial barrier
  imposed by previous standards between production and the rest of the life cycle.
- It defines an innovative community-based certification procedure
  where hardware device can get certified
  as "open source hardware according to DIN SPEC 3105"
  without engaging costly audits or certification fees.
  Products can get certified by reviewers from the OSH community
  in a transparent and public process.

> **NOTE**\
Bonvoisin, J., Molloy, J., Häuer, M. and Wenzel, T., 2020.
Standardisation of Practices in Open Source Hardware.
Journal of Open Hardware, 4(1), p.2.
DOI: <http://doi.org/10.5334/joh.22>

## I am a hardware developer; why would I align with this standard?

To build trust with the community around your project or hardware.
DIN SPEC 3105 provides a clear reference for you to
flag your community members what you mean with open source.
You don't have to explain it,
just to say "this project is open source in the sense of DIN SPEC 3105".

More, you can get your hardware certified by the community
and therewith leave no doubt that your hardware is *really* open.
DIN SPEC 3105-2 defines a community-based certification process
that allows you to get the label "Open source according to DIN SPEC 3105"
in full transparency and free of charge.

You are interested in taking part in the first certification wave?
Contact us at [assessment@oho.wiki](mailto:assessment@oho.wiki)

## Why do we need this standard?

Clear definitions contribute to establish mutual trust.
A more precise definition of Open Source Hardware
means a better common ground for the OSH community to chart a shared identity.
It helps strengthening the OSH movement
and building up trust with other stakeholders outside this community
(businesses, policy makers, etc.).
It makes sure that the engagement of the OSH community
doesn't get washed away by "openwashing" discourses.

## Why does DIN SPEC 3105 write a bit of History?

- A more precise definition of Open Source Hardware
  means a better common ground for the OSH community to chart a shared identity.
  This document helps building up the OSH movement
  and building up trust with other stakeholders outside this community
  (businesses, policy makers, etc.).
- This is the first standard ever to be published by a national standardisation body
  under an open source license.
  This document is an innovation itself
  in terms of transparency of standardisation process.
- Further version of the standard can be developped in an open standardisation process
  where anybody can participate.
  If you would like to improve this standard
  and participate to the establishment of a new version,
  [visit this page](https://gitlab.com/OSEGermany/OHS-3105/-/blob/develop/CONTRIBUTING.md).

## Why such a cryptic name "DIN SPEC 3105"?

Let's answer this in three times:

- **"DIN"** is the name of the German Institute for Standardisation
 (in German, "Deutsche Institut für Normung").
 They hosted the process of writing this standard.
- **"SPEC"** is a type of document they publish.
 A "SPEC" is not exactly speaking a "standard" as they define it
 but is some kind of small version of a standard, so to say.
 Such a document is agreed upon by a working group in a so-called
 "Publicly Available Specification".
- **"3105"** is just a number and has no meaning itself.

## Does DIN SPEC 3105 integrate requirements about repairability or circularity?

No, DIN SPEC 3105 is purely about documentation
and not about the product design itself.
It does not intend to restrict the creative freedom of authors
or to enforce product design practices.
That said, DIN SPEC 3105 ...

> adopts a product life cycle perspective,
considering that "making" a product is not only about manufacturing it,
it is also about using, maintaining, updating and processing it at end-of-life.
It defines documentation as information allowing to operate
all activities belonging to the product life cycle,
from raw material extraction to end-of-life.
Therewith, it drops the arbitrary and artificial barrier made by previous standards
between production and the rest of the life cycle.
[(source)](https://arxiv.org/abs/2004.07143)

# Governance and future plans

## Who wrote DIN SPEC 3105?

Writing DIN SPEC 3105 involved a formal consortium of 21 people from the OSH community
as well as more than 50 members of the mailing list
(see the list of authors in the standard itself).
More than 25 institutions were represented,
mostly from the EU, the US, but also Latin America.
This core maintained sustained interaction with relevant members of the OSH community
(including people from OSHWA)
to discuss drafts and concepts to be included in the document.
The project of writing DIN SPEC 3105 started in March 2019.
The first version was officially published 26 June 2020 by DIN
via its edition branch Beuth Verlag.

## Why is DIN SPEC 3105 hosted by OSE Germany (OSEG)?

*Short answer:* Because OSEG initiated it.

*Long answer:* OSEG initiated and led the creation of DIN SPEC 3105.
However, DIN SPEC 3105 is neither owned by OSEG
nor the result of OSEGs work alone.
OSEG took over coordination with DIN
and moderated the writing process
involving [a lot of people from the field of Open Source Hardware](#who-wrote-din-spec-3105),
which are not affiliated in any way with OSEG.
In that sense,
OSEG is just one of the many members of the DIN SPEC 3105 working group.

OSEG also leads the creation of the first assessment body
implementing the peer-assessment process defined in DIN SPEC 3105.
This is the result of OSEGs willingness
to push forward practices in Open Source Hardware.
OSEG has no exclusive right to create such an assessment body.

OSEG welcomes anyone to participate in the further development of DIN SPEC 3105
and their related projects.

## Why are there two versions of the document?

Since the publication date of DIN SPEC 3105,
the document entered a community-based process of continuous improvement.
The officially published standard is accessible free of charge
from DIN's partnering publisher Beuth Verlag
(see [DIN SPEC 3105-1](https://www.beuth.de/en/technical-rule/din-spec-3105-1/324805763)
and [DIN SPEC 3105-2](https://www.beuth.de/en/technical-rule/din-spec-3105-2/324805750)).
But at the same time, the constantly progressing "community version"
of the standard is maintained in the present repository.
Doing so, we keep an official reference
and can work as a community on the further development of the standard
in full transparency.

## Are there plans to get the SPEC adopted by other standards bodies, like the ISO?

*Short answer:* Yes, but nothing specific yet.

*Long answer:*
Yes, although it is not necessary for DIN SPEC 3105
to be acknowledged worldwide. \
It is an official *German* standard,
but there is no geographical limit to its adoption.
Converting DIN SPEC 3105 into an ISO standard would increase its visibility & authority,
but *compliance to standards is voluntary* anyway.
So, how far the standard is adopted in practice,
mainly depends on the willingness of the Open Source Hardware community. \
Also, such a step is bound to significant rewriting and discussion efforts,
which may take several years.

## How does DIN SPEC 3105 relate to other standards and initiatives?

### DIN SPEC 3105-1 and the OSHWA-hosted definition

DIN SPEC 3105 extends the OSHWA-hosted [Open Source Hardware Definition](https://www.oshwa.org/definition/)
which itself extends the [Open Source Definition](https://opensource.org/osd)
from the Open Source Initiative.
While the OSHWA-hosted definition focuses mainly
on aspects of documentation and hardware *licensing*,
DIN SPEC 3105 focuses on documentation *contents*.
Together, these documents deliver an unambiguous definition
of the term Open Source Hardware
based on objective and enforceable criteria.

### DIN SPEC 3105-2 attestation and the OSHWA certification program

DIN SPEC 3105-2 defines an assessment process
that has a larger scope, is more transparent and is more thorough.

OSHWA provides a [self-certification scheme](https://certification.oshwa.org/)
that naturally builds upon the [OSHWA-hosted Definition](https://www.oshwa.org/definition/).
Like the OSHWA-hosted definition,
the self-certification program is more focused on licensing
and aspects of intellectual property
than on the contents of the hardware documentation.
OSHWA members may review the quality of documentation of submitted projects
as part of the certification process
but the criteria used are not formalised.
They may have internal guidelines and checklists defining
what is minimal acceptable OSH documentation, but these are not publicly available.

DIN SPEC 3105-2 defines a community-based assessment process
where *both* documentation contents and licensing aspects are audited.
This process is transparent since reviews are publicly available.
It is also more thorough,
since it is based on actual documentation and product reviews.

**Why this weird designation "attestation" while OSHWA talks about certification?**

"Certification" is not a protected term, unless anywhere officially defined.
Hence anyone can print and offer his/her individual certificate at home
(as people do e.g. for seminars), that's totally legit, but you cannot
issue yourself an ISO 9001 certificate, that would be fraud.
OSHWA is free to offer an OSHWA certificate;
their definition is not connected to international standards.
However, DIN SPEC 3105-2 _is_, so other rules apply.

In internationally defined terms (e.g. DIN EN ISO-IEC 17000) DIN SPEC 3105-2
does define an assessment procedure (that differs from conventional procedures
since it's community-based). For certification procedures different requirements
apply (e.g. the necessity of accredited certification bodies, which we clearly
wanted to avoid in this draft to prevent power concentrations).

### DIN SPEC 3105 & Open Know-How

> Open Know-How is a community of open hardware organisations and individuals
> setting new standards to expand knowledge, enable collaboration,
> and accelerate innovation in research, design and manufacturing.
[(source)](https://openknowhow.org/)

The Open Know-How Working Group published 2019
the [Open Know-How Manifest Specification Version 1.0](
https://app.standardsrepo.com/MakerNetAlliance/OpenKnowHow/src/branch/master/1).
This standard defines a way to improve discoverability of open hardware
through standardised metadata format.

> The working principle of this standard is
> creating an additional file, called a “manifest”,
> for each OSH project documentation, which contains metadata
> relative to the documentation and to other files commonly shared
> alongside technical documentation,
> e.g. license terms and the descriptive “readme” file. [(source)](https://arxiv.org/abs/2004.07143)

This standard addresses metadata format
while DIN SPEC 3105 addresses aspects of documentation contents and licensing.
These documents are complementary and compatible with each other.

Some of the authors of DIN SPEC 3105
are also members of the Open Know-How (OKH) working group.

### DIN SPEC 3105-1 & Open Repair Data Standard

The [Open Repair Data Standard](https://openrepair.org/news/open-repair-data-standard-version-0-1/)
aims to facilitate exchange of technical data between stakeholders of product repair.
In this context, technical data is expected to be generated by repairers,
while in DIN SPEC 3105 documentation is expected to be generated
by the author of the piece of hardware,
or in specific cases, by a close collaborator acting as documenter.
Therewith, the Open Repair Data standard
covers only one specific phase of the product life cycle
and concerns retro-engineered documentation
instead of original documentation from the authors.

# Participating

## How can I ask a question?

1. Look if the question has already been asked and answered,
   either here in this FAQ, or in [the issues](https://gitlab.com/OSEGermany/OHS-3105/-/issues)
2. If not, ask your question [in a new issue](https://gitlab.com/OSEGermany/OHS-3105/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

Depending on the question, it might remain an issue,
or make its way into a FAQ, later on.

## How can I contribute?

Check our [contribution guide](https://gitlab.com/OSEGermany/OHS-3105/-/blob/develop/CONTRIBUTING.md).
It tells about the governance adopted in this project
and how to take part in the community.
If you want to get familiar with contributing to an open source project,
check GitLab's [Fearless Contribution: A Guide for First-Timers](
https://about.gitlab.com/blog/2016/06/16/fearless-contribution-a-guide-for-first-timers/).

## I think something should be changed in the SPEC; how do I go about it?

The general rule of contributing guidelines apply:

- If you have a concrete suggestion for a change,
  suggest it by doing a merge request.
  <!--
    TODO: add link to a mini-tutorial for how to suggest a change/make a merge-request,
    including making a GitLab account
    and then clicking on the edit button on the markdown document
   -->
- If you do *not* have a concrete suggestion,
  [open a new issue](
  https://gitlab.com/OSEGermany/OHS-3105/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

# OSH Project development & certification

## How should I ...?

For all your questions about how to setup, develop and lead your project,
please refer to our [Best Practice for OSH development](
DIN_SPEC_3105-1_best-practice.md) document.
It includes info about documentation, licensing, storage
and even partly about certification under this specification.

## I would like to get my hardware certified; where should I begin?

See the [Certification section](
DIN_SPEC_3105-1_best-practice.md#certification)
in our [Best Practice for OSH development](
DIN_SPEC_3105-1_best-practice.md) document.
