<!--
SPDX-FileCopyrightText: 2020 Robin Vobruba <hoijui.quaero@gmail.com>
SPDX-FileCopyrightText: 2020 Jérémy Bonvoisin <jbjb2971@bath.ac.uk>

SPDX-License-Identifier: CC0-1.0
-->

# Welcome to DIN SPEC 3105's auxiliary documents

[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-blue.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/OSEGermany/OHS-3105-aux)](https://api.reuse.software/info/gitlab.com/OSEGermany/OHS-3105-aux)
[![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org)

You will find here
the latest version all the documents that help in understanding
and interpreting the [DIN SPEC 3105](https://gitlab.com/OSEGermany/OHS/)

Quick guide:

- You don't know what DIN SPEC 3105 is?
  Check our [FAQ](FAQ.md).
- You are an Open Source hardware developer and would like to know
  what DIN SPEC 3105 means for your hardware?
  Check our [best-practice guide](DIN_SPEC_3105-1_best-practice.md)
- You would like to participate in the development
  of the next version of DIN SPEC 3105?
  Check the main repos [contribution guide](
  https://gitlab.com/OSEGermany/OHS/-/blob/develop/CONTRIBUTING.md).

You may also want to check out the [What is it? section](
https://gitlab.com/OSEGermany/OHS/-/blob/develop/README.md#what-is-it)
of the main repos README.

## Source & Export

The main (source) files and their generated versions:

| | main | specific version |
| ---- | --- | --- |
| _git reference_ | [`master`](https://gitlab.com/OSEGermany/OHS-3105-aux/-/tree/master/) | [`${PROJECT_VERSION}`](.) |
| __FAQ__ |  [source](https://gitlab.com/OSEGermany/OHS-3105-aux/-/tree/master/FAQ.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/master/FAQ.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/master/FAQ.pdf) | [source](FAQ.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/FAQ.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/FAQ.pdf) |
| __OSH Best Practise__ | [source](https://gitlab.com/OSEGermany/OHS-3105-aux/-/tree/master/DIN_SPEC_3105-1_best-practice.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/master/DIN_SPEC_3105-1_best-practice.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/master/DIN_SPEC_3105-1_best-practice.pdf) | [source](DIN_SPEC_3105-1_best-practice.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/DIN_SPEC_3105-1_best-practice.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/DIN_SPEC_3105-1_best-practice.pdf) |
| __MEETME__ | [source](https://gitlab.com/OSEGermany/OHS-3105-aux/-/tree/master/MEETME.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/master/MEETME.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/master/MEETME.pdf) | [source](MEETME.md) - [HTML](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/MEETME.html) - [PDF](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/MEETME.pdf) |
| ___all output___ | [index](https://osegermany.gitlab.io/OHS-3105-aux/master/index.html) | [index](https://osegermany.gitlab.io/OHS-3105-aux/${PROJECT_VERSION}/index.html) |

## Development

### Get the documentation build-tool {#get-build-tool}

Make sure you have the [MoVeDo](https://github.com/movedo/MoVeDo)
documentation build tool checked out:

If you have not yet cloned this repo,
you can do so,
plus get MoVeDo in one go with:

```bash
git clone --recurse-submodules https://gitlab.com/OSEGermany/OHS-3105-aux.git
cd OHS-3105-aux
```

If you already have this repo available locally,
you can switch to it and get MoVeDo like this:

```bash
cd OHS-3105-aux
git submodule update --init --recursive
```

### How to generate output

If you have MoVeDo available locally as [described above](#get-build-tool),
simply run:

```bash
movedo/scripts/build
```

## Imprint

<https://www.ose-germany.de/impressum/>
